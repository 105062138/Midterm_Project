# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer
    <link href="style.css" type="text/css" rel="stylesheet">

    <script src="config.js"></script>
    <script src="index.js"></script>
## Topic
* [Chat room]
* Key functions (add/delete)
    1. [chat]
    2. [load message history]
    3. [chat with new user ]
* Other functions (add/delete)
    1. [登入頁面背景互動]
    2. [防止空白訊息傳出]
    3. [顯示當前使用帳號]
    


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
一打開網頁即是登入頁面，左上角會向使用者提出要求通知的權限。<br>
背景是由javascript寫成的互動小遊戲，使用者能夠移動游標與點擊<br>滑鼠左鍵，與之做互動。

<br>
<img src="login_page.PNG" width="500px" height="250px"></img><br>
<br>
登入功能
<br>
<img src="signin.PNG" width="200px" height="250px"></img><br>
<br>
(1) Login:提供使用者使用已經sign-up過的帳號登入<br>
-----------------------
(2) Sign-up:提供使用者註冊帳號<br>
------------------------
(3) Google登入<br>
----------------------------------------
<p>-----------------------------------------------------</p>
登入成功後，隨即進入聊天室頁面
---------------------
<br>
<img src="chat_noti.PNG" width="600px" height="300px"></img><br>
<br>
(1)登入後顯示當前使用的帳號，避免註冊兩個帳號以上的使用者造成使用上的混亂。<br>
(2)登出按鈕，按下即跳轉到登入畫面。
<br>
(3)輸入訊息與傳送，為方便使用者操作，不用每次傳訊息都按下send，按下enter鍵也能將訊息傳出。
<br>
note:空白訊息則不予傳出。
<br>
(4)聊天室滾輪，能上下滑動，觀看歷史訊息。載入完歷史訊息後滾輪會置底。
<br>
(5)背景為CSS animation 會不斷的向左方滾動。<br>
(6)假使您允許該網站向您發出通知，則有新訊息穿出時會有通知方塊彈出。<br>
<br>
<br>

當前使用者的信息會出現在右方，並以黃色底做為區別。<br>
非當前使用者的訊息則會出現在左方，並顯示其使用帳號。<br><br>

由於有判斷currentUser使用的帳號與database紀錄的帳號是否一樣，<br>
所以即使重整頁面，當前使用者的歷史訊息仍舊會出現在右邊。<br>
--------

<br>
<br>
still working good on mobile device<br><br>
<img src="m0.jpg" width="100px" height="150px"></img><br>
<br>
<img src="m1.jpg" width="100px" height="150px"></img><br>
<br>

<p>---------------------------------------------------------</p>
## Security Report (Optional)<br>
(1)使用firebase Authentication<br>
         需要登入才能夠進入聊天室<br>
    意即沒有登入無法使用該網站(聊天室)的功能<br>
    增加網頁的安全性<br>
(2) 即使直接在網址打上https://105062138.gitlab.io/Midterm_Project/chat.html<br>
網頁仍會判斷未登入者，並導向登入頁面。<br>
(3)<br>
<img src="safe.PNG" width="500px" height="10px"></img><br>
<br>
網頁為安全。

